
import java.io.*;

public class Ejercicio2 {

    final static String FICHERO_NUMEROS = "randoms.txt";
    final static String RUTA_RANDOM = "out/artifacts/Random10_jar/ejemploEj2PSP.jar";

    public static void main(String[] args){

        Process process = null;
        try {
            process = new ProcessBuilder("java", "-jar", RUTA_RANDOM).start();
        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(1);
        }

        OutputStreamWriter osw = new OutputStreamWriter(process.getOutputStream());
        InputStreamReader isr = new InputStreamReader(process.getInputStream());

        try (BufferedWriter bfw = new BufferedWriter(osw);
             BufferedReader bfr = new BufferedReader(isr);
             FileWriter fw = new FileWriter(FICHERO_NUMEROS)) {

            BufferedReader bfrcom = new BufferedReader(new InputStreamReader(System.in));

            System.out.println("Empieza el programa");
            System.out.println("(Escribe un texto cualquiera o 'Stop' si quieres terminar el programa): ");
            System.out.println("obtendras un random entre 1 y 10: ");
            System.out.println(" ");


            String line;
            boolean bucle;
            do {
                System.out.print("Escribir un texto: ");

                line = bfrcom.readLine();

                bfw.write(line);
                bfw.newLine();

                bfw.flush();

                bucle = !line.equalsIgnoreCase("stop");
                if (bucle) {
                    String number = bfr.readLine();
                    System.out.println(number);
                    fw.write(number + System.lineSeparator());
                }
            } while (bucle);

        } catch (IOException e) {
            System.err.println("IO Exception: " + e.getMessage());
            System.exit(2);
        } finally {
            int exitValue = 0;
            try {
                exitValue = process.waitFor();
            } catch (InterruptedException e) {
                System.err.println("InterruptedException: " + e.getMessage());
                System.exit(3);
            }
            System.out.println(System.lineSeparator() + "Los resultados se guardan en: " + FICHERO_NUMEROS);
            if (exitValue != 0) {
                System.err.println("Error ha fallado Random10: ");
                System.out.println(process.getErrorStream());
            }
        }
    }

}
