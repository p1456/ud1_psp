import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    final static int TIEMPO=2;
    final static String FICHERO = "output.txt";
    public static void main(String[] args) throws IOException {
        if (args.length <= 0) {
            System.err.println("I need a command/program to execute");
            System.exit(-1);
        }


        List<String> argList = new ArrayList<>(Arrays.asList(args));

        ProcessBuilder pb = new ProcessBuilder(argList);

        try {
            Process process = pb.start();
            if (!process.waitFor(TIEMPO, TimeUnit.SECONDS)) {
                System.err.println("El proceso hijo ha tardado mucho");

            } else {

                InputStream inStream = process.getInputStream();

                InputStreamReader inputStreamReader = new InputStreamReader(inStream);
                BufferedReader bufReader = new BufferedReader(inputStreamReader);
                BufferedReader bf = new BufferedReader(new InputStreamReader(process.getInputStream()));
                FileWriter fw = new FileWriter(FICHERO);

                System.out.println("Output del proceso hijo: " + Arrays.toString(args) + " : ");

                String line;
                while ((line = bufReader.readLine()) != null) {
                    System.out.println(line);
                    fw.write(line + System.lineSeparator());
                }
                while ((line = bf.readLine()) != null) {
                    System.out.println(line);
                    fw.write(line + System.lineSeparator());
                }
                System.out.println("Los resultados se guardan en: " + FICHERO);

            }
        }catch (IOException ex){
            System.err.println("IO Exception");
            System.exit(-1);
        }catch (InterruptedException ex){
            System.err.println("The child process exited with error");
            System.exit(-1);
        }


    }
}





